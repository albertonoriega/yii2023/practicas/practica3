﻿DROP DATABASE IF EXISTS practica3Yii;

CREATE DATABASE practica3Yii;

USE practica3Yii;

CREATE TABLE catalogo (
id int AUTO_INCREMENT,
  nombre varchar(100) ,
  descripcion varchar(30),
  PRIMARY KEY (id) 
); 

INSERT INTO catalogo VALUES
( 1, 'flor1', 'Flor de campo'),
( 2, 'flor2', 'Flor de bosque');